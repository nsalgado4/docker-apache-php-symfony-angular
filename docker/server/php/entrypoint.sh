#!/usr/bin/env bash

COMPOSER_MEMORY_LIMIT=-1 composer install -n --optimize-autoloader

exec "$@"