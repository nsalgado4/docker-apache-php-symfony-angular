import {Component, OnInit} from '@angular/core';
import {HealthCheckService} from "./health-check.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  code = '';
  title = 'frontend';
  status = '';

  constructor(private healthCheckService: HealthCheckService) {
  }

  ngOnInit() {
    this.getHealthCheck();
  }

  getHealthCheck() {
    this.healthCheckService
      .fetchHealthCheck()
      .subscribe((data: any) => {
        this.code = data.code;
        this.status = data.status;
      });
  }
}
