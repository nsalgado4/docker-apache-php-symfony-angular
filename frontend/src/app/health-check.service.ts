import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HealthCheckService {

  API_URL: string = "http://www.xinfo-web.test:8001/api/v1/";

  constructor(private httpClient: HttpClient) {
  }

  fetchHealthCheck(): Observable<any> {
    return this.httpClient.get(this.API_URL + 'healthCheck', {headers: {Accept: 'application/json'}});
  }
}
